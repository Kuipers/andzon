/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openremote.controller.protocol.somfy.rts.*;

/**
 *
 * @author mark
 */
 
/**
 * All KNX tests aggregated here.
 *
 * @author <a href="mailto:juha@openremote.org">Juha Lindfors</a>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
   {
       SerialCommunicatorTest.class,
       TimeOutTimerTest.class,
       SomfyRTSAddressConverterTest.class,
       SomfyRTSDriverTest.class,
       CommanderHandlerTest.class,
       SomfyRTSCommanderTest.class,
       SomfyRTSCommanderBuilderTest.class
           
   }
)
public class SomfyTests {
        
}
