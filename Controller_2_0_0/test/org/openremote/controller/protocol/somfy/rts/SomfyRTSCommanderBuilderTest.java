package org.openremote.controller.protocol.somfy.rts;

import gnu.io.SerialPort;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Element;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openremote.controller.command.Command;

public class SomfyRTSCommanderBuilderTest {

    private static final int BAUD_RATE = 9600;
    private static final int DATABITS = SerialPort.DATABITS_8;
    private static final int STOPBITS = SerialPort.STOPBITS_1;
    private static final int PARITY = SerialPort.PARITY_NONE;
    private SomfyRTSCommanderBuilder builder;
    private SerialCommunicatorMock mSerialCommunicatorMock;

    @Before
    public void setUp() throws Exception {
        mSerialCommunicatorMock = new SerialCommunicatorMock(BAUD_RATE, DATABITS, STOPBITS, PARITY);
        builder = new SomfyRTSCommanderBuilder(mSerialCommunicatorMock);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * This test will check if the SomfyRTSCommanderBuilder build the objects as
     * expected.
     */
    @Test
    public void test_build_a_SomfyRTSCommander_object() {
        SomfyRTSCommander commander = getCommander("1.1", RTSActionType.ACTION_UP);
        assertNotNull("Commander is not build", commander);
        assertEquals("busID is not as expected", "1.1", commander.getBusID());
        assertEquals("busID is not as expected", "up", commander.getActionType());
        assertNotNull("driver is not as expected", commander.getSomfyRTSDriver());
        assertNotNull("handler is not as expected", commander.getCommandHandler());
    }

    @Test
    public void test_create_commander_and_call_send() {
        String busID = "1.1";
        mSerialCommunicatorMock.setBusID(busID);
        SomfyRTSCommander commander = getCommander(busID, RTSActionType.ACTION_UP);
        for (int i = 0; i < 3; i++) {
            commander.send();
            sleep(100);
        }
        
        int sleep = 100;
        int counter = 0;
        while (counter < sleep) {
            sleep(100);
            counter++;
        }
        sleep(1000);
        assertEquals("CommanderHandler queue should be 0", 0, builder.getCommanderHandler().getQueueCount());
    }
    
    @Test
    public void test_create_commander_and_call_send_for_real() {
        String busID = "1.1";
        //mSerialCommunicatorMock.setBusID(busID);
        
        builder = new SomfyRTSCommanderBuilder();
        SomfyRTSCommander commanderUP = getCommander(busID, RTSActionType.ACTION_UP);
        commanderUP.send();

        busID = "1.4";
        SomfyRTSCommander commanderSTOP = getCommander(busID, RTSActionType.ACTION_STOP);
        commanderSTOP.send();

        busID = "1.3";
        SomfyRTSCommander commanderDOWN = getCommander(busID, RTSActionType.ACTION_DOWN);
        commanderDOWN.send();

        int sleep = 100;
        int counter = 0;
        while (counter < sleep) {
            sleep(100);
            counter++;
        }
        sleep(10000);
    }
    
    private SomfyRTSCommander getCommander(String busID, String actionType) {
        Element ele = new Element("command");
        ele.setAttribute("id", "test");
        ele.setAttribute("protocol", "SomfyRTS");
        ele.setAttribute(Command.DYNAMIC_VALUE_ATTR_NAME, "xx");

        Element propName = new Element("property");
        propName.setAttribute("name", "name");
        propName.setAttribute("value", "testName");

        Element propBusID = new Element("property");
        propBusID.setAttribute("name", "busID");
        propBusID.setAttribute("value", busID);

        Element propActionType = new Element("property");
        propActionType.setAttribute("name", "actionType");
        propActionType.setAttribute("value", actionType);

        ele.addContent(propName);
        ele.addContent(propBusID);
        ele.addContent(propActionType);

        return (SomfyRTSCommander) builder.build(ele);
    }

    private void sleep(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException ex) {
            Logger.getLogger(TimeOutTimerTest.class.getName()).log(Level.SEVERE, "Something bad happend when thread was sleeping...", ex);
        }
    }
}
