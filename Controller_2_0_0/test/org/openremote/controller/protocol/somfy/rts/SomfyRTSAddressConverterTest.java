/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author mark
 */
public class SomfyRTSAddressConverterTest {
    
    public SomfyRTSAddressConverterTest() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void test_convert_bus_to_USB_port() {
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB0",SomfyRTSAddressConverter.getSerialPort("1.1"));
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB0",SomfyRTSAddressConverter.getSerialPort("2.1"));
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB0",SomfyRTSAddressConverter.getSerialPort("3.1"));
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB1",SomfyRTSAddressConverter.getSerialPort("4.1"));
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB1",SomfyRTSAddressConverter.getSerialPort("5.1"));
        assertEquals("SerialPort is not the expected port", "/dev/ttyUSB1",SomfyRTSAddressConverter.getSerialPort("6.1"));
    }
    
    @Test
    public void test_convert_bus_to_prefix_with_action_type() {
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'A', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("1.1", RTSActionType.ACTION_UP));
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'B', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("2.1", RTSActionType.ACTION_UP));
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'C', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("3.1", RTSActionType.ACTION_UP));
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'A', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("4.1", RTSActionType.ACTION_UP));
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'B', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("5.1", RTSActionType.ACTION_UP));
        assertArrayEquals("Received prefix is not as expected", new byte[]{(byte)'C', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand("6.1", RTSActionType.ACTION_UP));
    }
    
    @Test
    public void test_convert_actiontype_to_command() {
        String busID = "1.1";
        assertArrayEquals("Received command is not as expected", new byte[]{(byte)'A', (byte)0x10},SomfyRTSAddressConverter.getSerialCommand(busID, RTSActionType.ACTION_UP));
        assertArrayEquals("Received command is not as expected", new byte[]{(byte)'A', (byte)0x40},SomfyRTSAddressConverter.getSerialCommand(busID, RTSActionType.ACTION_DOWN));
        assertArrayEquals("Received command is not as expected", new byte[]{(byte)'A', (byte)0x80},SomfyRTSAddressConverter.getSerialCommand(busID, RTSActionType.ACTION_STOP));
        assertArrayEquals("Received command is not as expected", new byte[]{(byte)'A', (byte)0x20},SomfyRTSAddressConverter.getSerialCommand(busID, RTSActionType.ACTION_SELECT));
    }
    
    @Test
    public void test_convert_command_to_ledID() {
        assertEquals("Received LED is not as expected", SomfyRTSAddressConverter.LEDS.LED1, SomfyRTSAddressConverter.getLedIDFromData(new byte[]{(byte)'a', (byte)0x0E}));
        assertEquals("Received LED is not as expected", SomfyRTSAddressConverter.LEDS.LED2, SomfyRTSAddressConverter.getLedIDFromData(new byte[]{(byte)'a', (byte)0x0D}));
        assertEquals("Received LED is not as expected", SomfyRTSAddressConverter.LEDS.LED3, SomfyRTSAddressConverter.getLedIDFromData(new byte[]{(byte)'a', (byte)0x0B}));
        assertEquals("Received LED is not as expected", SomfyRTSAddressConverter.LEDS.LED4, SomfyRTSAddressConverter.getLedIDFromData(new byte[]{(byte)'a', (byte)0x07}));
        assertEquals("Received LED is not as expected", SomfyRTSAddressConverter.LEDS.LED_ALL, SomfyRTSAddressConverter.getLedIDFromData(new byte[]{(byte)'a', (byte)0x00}));
    }

    @Test
    public void test_get_all_pins_low_command() {
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'A', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("1.1"));
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'B', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("2.1"));
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'C', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("3.1"));
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'A', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("4.1"));
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'B', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("5.1"));
        assertArrayEquals("Received pin low is not as expected", new byte[]{(byte)'C', (byte)0x00},SomfyRTSAddressConverter.getPortAllPinsLowCommand("6.1"));
    }

    @Test
    public void test_get_LED_ID_from_busID() {
        assertEquals("Failed converting busID to LED ID", SomfyRTSAddressConverter.LEDS.LED1,SomfyRTSAddressConverter.getLedIDFromBusID("1.1"));
        assertEquals("Failed converting busID to LED ID", SomfyRTSAddressConverter.LEDS.LED2,SomfyRTSAddressConverter.getLedIDFromBusID("1.2"));
        assertEquals("Failed converting busID to LED ID", SomfyRTSAddressConverter.LEDS.LED3,SomfyRTSAddressConverter.getLedIDFromBusID("2.3"));
        assertEquals("Failed converting busID to LED ID", SomfyRTSAddressConverter.LEDS.LED4,SomfyRTSAddressConverter.getLedIDFromBusID("2,4")); // test a ,
        assertEquals("Failed converting busID to LED ID", SomfyRTSAddressConverter.LEDS.LED_ALL,SomfyRTSAddressConverter.getLedIDFromBusID("2,5")); // test a ,
    }
}
