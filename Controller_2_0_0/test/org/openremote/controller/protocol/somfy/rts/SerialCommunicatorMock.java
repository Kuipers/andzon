/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.security.InvalidParameterException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mark
 */
public class SerialCommunicatorMock extends SerialCommunicator{
    
        private String mSerialPort = "";
        private String mWrittenData = "";
        private byte[] mWrittenBytes = new byte[0];
        private boolean mForceTimeOut = false;
        private int mSelectedObject = 0;
        private String mBusID = "";
        private boolean mPortClosed = true;

        public void setBusID(String busID) {
            mBusID = busID;
        }

        public SerialCommunicatorMock(int baudRate, int databits, int stopbits, int parity) {
            super(baudRate, databits, stopbits, parity);
        }

        @Override
        public boolean openPort(String serialPort) throws InvalidParameterException {
            mSerialPort = serialPort;
            mPortClosed = false;
            return true;
        }

        public boolean isPortClosed() {
            return mPortClosed;
        }

        public String getSerialPort() {
            return mSerialPort;
        }

        @Override
        public boolean closePort() {
            mPortClosed = true;
            return true;
        }

        public void forceTimeOut(boolean forceTimeOut) {
            mForceTimeOut = forceTimeOut;
        }

        @Override
        public boolean write(byte[] bytes, int length) {
            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "Writing \"{0}\" to {1}", new Object[] {new String(bytes), mSerialPort});
            mWrittenBytes = bytes;
            if (!mForceTimeOut) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        byte[] selectCommand = SomfyRTSAddressConverter.getSerialCommand(mBusID, RTSActionType.ACTION_SELECT);
                        if (mWrittenBytes[mWrittenBytes.length -1] == (selectCommand[selectCommand.length -1])) {
                            byte[] selectedObject = getSelectedObject();
                            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "notify observers with {0}", selectedObject);
                            notifyObservers(selectedObject);
                        } else {
                            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "Action is not a ACTION_SELECT[{1}] but a action {2}[{0}]", new Object[] {mWrittenData, SomfyRTSAddressConverter.getSerialCommand(mBusID, RTSActionType.ACTION_SELECT), SomfyRTSAddressConverter.getActionType(mWrittenBytes)});
                        }
                    }
                }).start();
            }
            return true;
        }
        
        @Override
        public boolean write(String data) {
            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "Writing \"{0}\" to {1}", new Object[] {data, mSerialPort});
            mWrittenData = data;
            if (!mForceTimeOut) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (mWrittenData.equals(SomfyRTSAddressConverter.getSerialCommand(mBusID, RTSActionType.ACTION_SELECT))) {
                            byte[] selectedObject = getSelectedObject();
                            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "notify observers with {0}", selectedObject);
                            notifyObservers(selectedObject);
                        } else {
                            Logger.getLogger(SerialCommunicatorMock.class.getName()).log(Level.INFO, "Action is not a ACTION_SELECT[{1}] but a action [{0}]", new Object[] {mWrittenData, SomfyRTSAddressConverter.getSerialCommand(mBusID, RTSActionType.ACTION_SELECT)});
                        }
                    }
                }).start();
            }
            return true;
        }

        private byte[] getSelectedObject() {
            switch (mSelectedObject) {
                case 0: {
                    mSelectedObject++;
                    return new byte[] {(byte)'a', SomfyRTSAddressConverter.COMMAND_LED1};
                }
                case 1: {
                    mSelectedObject++;
                    return new byte[] {(byte)'a', SomfyRTSAddressConverter.COMMAND_LED2};
                }
                case 2: {
                    mSelectedObject++;
                    return new byte[] {(byte)'a', SomfyRTSAddressConverter.COMMAND_LED3};
                }
                case 3: {
                    mSelectedObject++;
                    return new byte[] {(byte)'a', SomfyRTSAddressConverter.COMMAND_LED4};
                }
                case 4: {
                    mSelectedObject = 0;
                    return new byte[] {(byte)'a', SomfyRTSAddressConverter.COMMAND_LED_ALL};
                }
                default:
                    return new byte[0];
            }
        }

        public String getWrittenData() {
            return mWrittenData;
        }

        public byte[] getWrittenBytes() {
            return mWrittenBytes;
        }
        
        @Override
        public boolean terminate() {
            return super.terminate();
        }
}
