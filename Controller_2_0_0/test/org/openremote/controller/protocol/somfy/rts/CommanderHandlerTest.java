/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author mark
 */
public class CommanderHandlerTest {

    private CommanderHandler mCommanderHandler;
    SomfyRTSCommanderMock mCommanderMock;

    public CommanderHandlerTest() {
    }

    @Before
    public void setUp() {
        mCommanderHandler = new CommanderHandler();
        mCommanderMock = new SomfyRTSCommanderMock(mCommanderHandler, null);
        mCommanderHandler.startHandling();
    }

    @After
    public void tearDown() {
        mCommanderHandler.stopHandling();
        mCommanderHandler.terminate();
    }

    @Test
    public void test_on_commanderhandling_finished_successful() {
        assertTrue("Failed to add commander to the queue", mCommanderHandler.putCommander(mCommanderMock));
        sleep(100);
        assertTrue("Commander startHandle is not called", mCommanderMock.mCommanderHandleCalled);
    }

    @Test
    public void test_on_command_handling_finished_and_start_next_commander() {
        SomfyRTSCommanderMock commander1 = new SomfyRTSCommanderMock(mCommanderHandler, null);
        SomfyRTSCommanderMock commander2 = new SomfyRTSCommanderMock(mCommanderHandler, null);

        assertTrue("Failed to add commander to the queue", mCommanderHandler.putCommander(commander1));
        assertTrue("Failed to add commander to the queue", mCommanderHandler.putCommander(commander2));
        sleep(100);
        assertEquals("Queue count is not as expected", 2, mCommanderHandler.getQueueCount());
        assertTrue("Commander startHandle is not called", commander1.mCommanderHandleCalled);
        assertFalse("Commander startHandle is not called", commander2.mCommanderHandleCalled);
        commander1.getHandlingCommanderCallback().onHandlingCommanderFinished(ErrorType.SUCCESS);
        sleep(100);
        assertEquals("Queue count is not as expected", 1, mCommanderHandler.getQueueCount());
        assertTrue("Commander startHandle is not called", commander2.mCommanderHandleCalled);
        commander2.getHandlingCommanderCallback().onHandlingCommanderFinished(ErrorType.SUCCESS);
        sleep(100);
        assertEquals("Queue count is not as expected", 0, mCommanderHandler.getQueueCount());
    }
    
    @Test
    public void test_on_command_handling_finished_but_failed_and_start_next_commander() {
        SomfyRTSCommanderMock commander1 = new SomfyRTSCommanderMock(mCommanderHandler, null);
        SomfyRTSCommanderMock commander2 = new SomfyRTSCommanderMock(mCommanderHandler, null);

        assertTrue("Failed to add commander to the queue", mCommanderHandler.putCommander(commander1));
        assertTrue("Failed to add commander to the queue", mCommanderHandler.putCommander(commander2));
        assertEquals("Queue count is not as expected", 2, mCommanderHandler.getQueueCount());
        sleep(1000);
        assertTrue("Commander startHandle is not called", commander1.mCommanderHandleCalled);
        assertFalse("Commander startHandle is not called", commander2.mCommanderHandleCalled);
        commander1.getHandlingCommanderCallback().onHandlingCommanderFinished(ErrorType.FAILED);
        sleep(1000);
        assertEquals("Queue count is not as expected", 1, mCommanderHandler.getQueueCount());
        assertTrue("Commander startHandle is not called", commander2.mCommanderHandleCalled);
        commander2.getHandlingCommanderCallback().onHandlingCommanderFinished(ErrorType.SUCCESS);
        sleep(1000);
        assertTrue("Commander startHandle is not called", commander2.mCommanderHandleCalled);
        assertEquals("Queue count is not as expected", 0, mCommanderHandler.getQueueCount());
    }

    private class SomfyRTSCommanderMock extends SomfyRTSCommander {

        public boolean mCommanderHandleCalled = false;

        public SomfyRTSCommanderMock(CommanderHandler handler, SomfyRTSDriver driver) {
            super(handler, driver);
        }
        
        public HandlingCommanderCallback getHandlingCommanderCallback() {
            return mHandlingCommanderCallback;
        }

        @Override
        public boolean startHandle(HandlingCommanderCallback callback) {
            System.out.println("startHandle");
            mHandlingCommanderCallback = callback;
            mCommanderHandleCalled = true;
            return true;
        }
    }

    private void sleep(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException ex) {
            Logger.getLogger(TimeOutTimerTest.class.getName()).log(Level.SEVERE, "Something bad happend when thread was sleeping...", ex);
        }
    }
}
