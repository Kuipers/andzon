package org.openremote.controller.protocol.somfy.rts;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SomfyRTSCommanderTest {

    private SomfyRTSCommander mRTSCommander;
    private CommanderHandlerMock mCommanderHandlerMock;
    private SomfyRTSDriverMock mRTSDriverMock;

    @Before
    public void setUp() throws Exception {
        mCommanderHandlerMock = new CommanderHandlerMock();
        mRTSDriverMock = new SomfyRTSDriverMock(null);
        mRTSCommander = new SomfyRTSCommander(mCommanderHandlerMock, mRTSDriverMock);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_start_handling_commander() {
        mRTSCommander.send();
        assertTrue("Commander is not added to the queue because putCommander is not called", mCommanderHandlerMock.mPutCommanderCalled);
        mCommanderHandlerMock.starthandlingCommander();
        assertNotNull("Callback may not be null", mRTSCommander.mHandlingCommanderCallback);
        assertTrue("OpenBusAsync of the RTSDriver is not called", mRTSDriverMock.mOpenBusAsyncCalled);
    }

    @Test
    public void test_onOpeningBusFinisched() {
        mRTSCommander.setActionType(RTSActionType.ACTION_UP);
        mRTSCommander.setBusID("1.1");
        mRTSCommander.setName("testName");
        mRTSCommander.send();
        mCommanderHandlerMock.starthandlingCommander();
        assertTrue("Commander is not added to the queue because putCommander is not called", mCommanderHandlerMock.mPutCommanderCalled);
        mRTSDriverMock.getBusOpenCallback().onOpeningBusFinished(ErrorType.SUCCESS);
        assertEquals("name is NOK", "testName", mRTSCommander.getName());
        assertEquals("busID is NOK, the send to the driver has failed?", "1.1", mRTSDriverMock.mBusIDMock);
        assertEquals("Send actionType is not as expected", RTSActionType.ACTION_UP, mRTSDriverMock.mSendActionType);
        assertTrue("CloseBus not called", mRTSDriverMock.mCloseBusCalled);
    }

    @Test
    public void test_send() {
        mRTSCommander.send();
        assertTrue("Commander is not added to the queue because putCommander is not called", mCommanderHandlerMock.mPutCommanderCalled);
    }

    private class SomfyRTSDriverMock extends SomfyRTSDriver {

        public String mSendActionType = "";
        public boolean mOpenBusAsyncCalled = false;
        public BusOpenCallback mBusOpenCallbackMock;
        public String mBusIDMock;
        public boolean mCloseBusCalled;
        
        public SomfyRTSDriverMock(SerialCommunicator communicator) {
            super(communicator);
        }

        @Override
        public boolean send(String actionType) {
            mSendActionType = actionType;
            return true;
        }

        @Override
        public boolean openBusAsync(String busID, BusOpenCallback callback) {
            mBusOpenCallbackMock = callback;
            mBusIDMock = busID;
            mOpenBusAsyncCalled = true;
            return true;
        }

        @Override
        public boolean closeBus() {
            mCloseBusCalled = true;
            return true;
        }
        
        public BusOpenCallback getBusOpenCallback() {
            return mBusOpenCallbackMock;
        }
    }

    private class CommanderHandlerMock extends CommanderHandler {
        public boolean mPutCommanderCalled = false;
        public ErrorType mHandlingCommanderFinishedCalled;
        
        @Override
        public boolean putCommander(SomfyRTSCommander commander) {
            mPutCommanderCalled = true;
            mCurrentCommander = commander;
            return true;
        }
        
        public void starthandlingCommander() {
            mCurrentCommander.startHandle(this);
        }

        @Override
        public boolean onHandlingCommanderFinished(ErrorType error) {
            mHandlingCommanderFinishedCalled = error;
            return true;
        }
    }
}
