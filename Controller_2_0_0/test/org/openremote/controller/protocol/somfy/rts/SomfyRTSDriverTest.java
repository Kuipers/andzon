/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import gnu.io.SerialPort;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mark
 */
public class SomfyRTSDriverTest implements BusOpenCallback {

    private static final String SERIAL_PORT = "/dev/ttyUSB0";
    private static final int BAUD_RATE = 9600;
    private static final int DATABITS = SerialPort.DATABITS_8;
    private static final int STOPBITS = SerialPort.STOPBITS_1;
    private static final int PARITY = SerialPort.PARITY_NONE;
    private SomfyRTSDiverMock mSomfyRTSDriver;
    private SerialCommunicatorMock mSerialCommunicator;
    private ErrorType mBusOpenSuccess;

    @Before
    public void setUp() {
        mBusOpenSuccess = ErrorType.INVALID;
        mSerialCommunicator = new SerialCommunicatorMock(BAUD_RATE, DATABITS, STOPBITS, PARITY);
        mSomfyRTSDriver = new SomfyRTSDiverMock(mSerialCommunicator);
    }

    @After
    public void tearDown() {
        mSerialCommunicator.terminate();
    }

    @Test
    public void test_send_action() {
        String busID = "1.1";
        mSerialCommunicator.setBusID(busID);
        byte[] expectedWrittenData = new byte[]{ (byte)'A', (byte)0x00};
        assertTrue("Failed start opening bus", mSomfyRTSDriver.openBusAsync(busID, this));
        assertTrue("Failed to send Action", mSomfyRTSDriver.send(RTSActionType.ACTION_UP));
        assertArrayEquals("Written data on the serial port is not as expected", expectedWrittenData, mSerialCommunicator.getWrittenBytes());
    }

    @Test
    public void test_open_bus_async_and_check_selected_Somfy_object() {
        String busID = "1.1";
        mSerialCommunicator.setBusID(busID);
        byte[] expectedWrittenData = new byte[]{ (byte)'A', (byte)0x00};
        byte[] expectedWrittenData2 = new byte[]{ (byte)'A', (byte)0x00};
        mSerialCommunicator.setBusID(busID);
        assertTrue("Failed start opening bus async", mSomfyRTSDriver.openBusAsync(busID, this));
        assertEquals("Serial port is not as expected", SERIAL_PORT, mSerialCommunicator.getSerialPort());
        assertArrayEquals("Action SELECT not send", expectedWrittenData, mSerialCommunicator.getWrittenBytes());
        assertFalse("Failed opening bus has timed out", mSomfyRTSDriver.hasTimedOut());
        assertArrayEquals("Action SELECT not send", expectedWrittenData2, mSerialCommunicator.getWrittenBytes());
    }

    // test the selection of different RTS objects!!!
    @Test
    public void test_open_bus_async_and_has_timed_out() {
        String busID = "1.1";
        mSerialCommunicator.setBusID(busID);
        byte[] expectedWrittenData = new byte[]{ (byte)'A', (byte)0x00};
        mSerialCommunicator.setBusID(busID);
        mSerialCommunicator.forceTimeOut(true);
        assertTrue("Failed start opening bus async", mSomfyRTSDriver.openBusAsync(busID, this));
        assertEquals("Serial port is not as expected", SERIAL_PORT, mSerialCommunicator.getSerialPort());
        assertArrayEquals("Action SELECT not send", expectedWrittenData, mSerialCommunicator.getWrittenBytes());
        sleep(2000);
        assertTrue("Failed opening bus has not timed out", mSomfyRTSDriver.hasTimedOut());
        assertEquals("Opening bus should not be successfull", ErrorType.FAILED, mBusOpenSuccess);
        assertTrue("SerialPort is not closed", mSerialCommunicator.isPortClosed());
    }

        // test the selection of different RTS objects!!!
    @Test
    public void test_open_bus_async_and_initialize() {
        SomfyRTSDriver driver = new SomfyRTSDriver();
        
        String busID = "1.4";
        mSerialCommunicator.setBusID(busID);
        assertTrue("Failed start opening bus async", driver.openBusAsync(busID, this));
        sleep(20000);
    }
    
    @Test
    public void test_close_bus() {
        assertTrue("Failed opening has not timed out", mSomfyRTSDriver.closeBus());
    }

    @Override
    public boolean onOpeningBusFinished(ErrorType error) {
        mBusOpenSuccess = error;
        return true;
    }

    private class SomfyRTSDiverMock extends SomfyRTSDriver {

        boolean mHasTimedOut = false;

        public SomfyRTSDiverMock(SerialCommunicator communicator) {
            super(communicator);
        }

        @Override
        public void onTimeOut() {
            System.out.println("onTimeOut called");
            mHasTimedOut = true;
            super.onTimeOut();
        }

        public boolean hasTimedOut() {
            return mHasTimedOut;
        }
    }

    private void sleep(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException ex) {
            Logger.getLogger(TimeOutTimerTest.class.getName()).log(Level.SEVERE, "Something bad happend when thread was sleeping...", ex);
        }
    }

}
