/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author mark
 *
 * This unit test will test the TimeOutTimer.
 */
public class TimeOutTimerTest {

    TimeOutTimer mTimer;
    TimeOutTimerCallback mCallback;
    boolean mOnTimeOutCalled;
            
    public TimeOutTimerTest() {
    }

    @Before
    public void setUp() {
        mTimer = new TimeOutTimer();
        mOnTimeOutCalled = false;
        mCallback = new TimeOutTimerCallback() {

            @Override
            public void onTimeOut() {
                mOnTimeOutCalled = true;
            }
        };
    }

    @After
    public void tearDown() {
        // release the object for next test
        mTimer.terminateTimer();
        mTimer = null;
        mCallback = null;
    }
    
     /**
     * Tests the start method for not excepting null as callback.
     * 
     * - the callback can not be null
     */
    @Test(expected=IllegalArgumentException.class)
    public void test_start_timer_with_null_as_callback(){
        mTimer.start(500, null);
    }

    /**
     * Tests the start method for accepting only positive integers
     * 
     * - the duration should not accept negative integers
     */
    @Test(expected=IllegalArgumentException.class)
    public void test_start_timer_with_negative_duration(){
        mTimer.start(-1, mCallback);
    }


    /**
     * Tests the timer to start counting and stopping the timer before it
     * exceeds. 
     * 
     * - start the timer with a timeout of 500ms
     * - wait+ for 400ms
     * - stop the timer
     * - the callback should not be called
     */
    @Test
    public void test_start_the_timer_and_stop_before_exceed() {
        int timeout = 500;
        int testDuration = 400;
        
        // start the timer and stop the timer before the end of the duration
        assertTrue("Start the timer has failed", mTimer.start(timeout, mCallback));
        sleep(testDuration);
        assertTrue("Stop the timer has failed", mTimer.stop());
        assertFalse("onTimeOut is called. This should not happen!", mOnTimeOutCalled);
    }
    
    /**
     * Tests the timer to start counting NOT stop the timer before it
     * exceeds. 
     * 
     * - start the timer with a timeout of 500ms
     * - wait for 600ms
     * - the callback should be called
     */
    @Test
    public void test_exceed_the_timer() {
        int timeout = 500;
        int testDuration = 600;
        
        // start the timer and stop the timer before the end of the duration
        assertTrue("Start the timer has failed", mTimer.start(timeout, mCallback));
        sleep(testDuration);
        assertTrue("onTimeOut is NOT called!", mOnTimeOutCalled);
    }

    @Test(expected=IllegalStateException.class)
    public void test_terminate_timer() {
        int timeout = 500;
        int testDuration = 200;
        
        // start the timer and stop the timer before the end of the duration
        assertTrue("Start the timer has failed", mTimer.start(timeout, mCallback));
        sleep(testDuration);
        assertTrue("Stop the timer has failed", mTimer.terminateTimer());
        
        // try start the timer this shoul end up in a IllegalStateException
        mTimer.start(timeout, mCallback);
    }

    private void sleep(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException ex) {
            Logger.getLogger(TimeOutTimerTest.class.getName()).log(Level.SEVERE, "Something bad happend when thread was sleeping...", ex);
        }
    }
}
