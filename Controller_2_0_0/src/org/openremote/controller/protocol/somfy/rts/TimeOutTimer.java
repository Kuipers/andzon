/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mark
 *
 * This timer is used as a timeout timer. When the timeout is called the
 * callback method will be called. When the timer is stopped before the timeout
 * there will nothing happen. Eventually this timer will be used for indication
 * if a message is received within a legal time.
 */
public class TimeOutTimer {

    private TimeOutTimerCallback mTimeOutTimerCallback;
    private Timer mTimer;
    private TimerTask mTimerTask;

    /**
     * Constructor
     */
    public TimeOutTimer() {
        mTimer = new Timer("TimeOut Timer");
    }
    
    /**
     * Start the timeout timer with to call callback.onTimeOut after a given
     * time.
     *
     * @param milliseconds Time in milliseconds
     * @param callback Expects a TimeOutTimerCallback
     * @return true if the method run successfully
     * @throws IllegalArgumentException If the callback is null or if the time
     * is negative
     * @throws IllegalStateException If the timer was already terminated
     */
    public boolean start(int milliseconds, TimeOutTimerCallback callback) throws IllegalArgumentException, IllegalStateException {
        if(callback == null) {
            throw new IllegalArgumentException("Callback cannot be null");
        }
        
        mTimeOutTimerCallback = callback;

        // create the timer task
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
            Logger.getLogger(TimeOutTimer.class.getName()).log(Level.SEVERE, "Timeout timer exceeded");
                mTimeOutTimerCallback.onTimeOut();
            }
        };
        
        // schedule the timer task
        mTimer.schedule(mTimerTask, milliseconds);
        
        Logger.getLogger(TimeOutTimer.class.getName()).log(Level.SEVERE, "Timeout of {0} started", milliseconds);
        return true;
    }

    /**
     * Stops the TimeOut from calling the callback.onTimeOut
     * @return true if the method has run successfully.
     */
    public boolean stop() {
        Logger.getLogger(TimeOutTimer.class.getName()).log(Level.SEVERE, "Timeout timer stopped");
        if (mTimerTask != null) {
            mTimerTask.cancel();
        }
        return true;
    }

    /**
     * Terminates the timer. It terminates the timer. callback.onTimeOut will
     * never be called anymore. If terminateTimer is called the timer can not be
     * used anymore.
     *
     * @return true if the method has run successfully
     */
    public boolean terminateTimer() {
        Logger.getLogger(TimeOutTimer.class.getName()).log(Level.SEVERE, "Terminate timer");
        if (mTimerTask != null) {
            mTimerTask.cancel();
        }
        if (mTimer != null) {
            mTimer.cancel();
        }
        return true;
    }
}
