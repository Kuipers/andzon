package org.openremote.controller.protocol.somfy.rts;

public interface BusOpenCallback {

    public boolean onOpeningBusFinished(ErrorType error);
}
