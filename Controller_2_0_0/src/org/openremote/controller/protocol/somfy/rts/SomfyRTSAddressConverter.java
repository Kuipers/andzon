/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author mark
 *
 * This address converter is explicitly for the Elexol USB I/O 24 R converter
 */
public class SomfyRTSAddressConverter {

    public static final char DEVICE_PORT1 = 'A';
    public static final char DEVICE_PORT2 = 'B';
    public static final char DEVICE_PORT3 = 'C';
    private static final char DEVICE_COMMAND_DIRECTION = '!';
    private static final char DEVICE_COMMAND_SCHMITT_TRIGGER = '$';
    private static final char DEVICE_COMMAND_ENABLE_MODE_2 = '2';
    private static final byte DEVICE_PORT1_DIRECTION_VALUE = (byte) 0x0F;
    private static final byte DEVICE_PORT2_DIRECTION_VALUE = (byte) 0x00;
    private static final byte DEVICE_PORT3_DIRECTION_VALUE = (byte) 0x00;
    private static final byte DEVICE_PORT1_SCHMITT_TRIGGER_VALUE = (byte) 0xF0;
    private static final byte DEVICE_PORT2_SCHMITT_TRIGGER_VALUE = (byte) 0x00;
    private static final byte DEVICE_PORT3_SCHMITT_TRIGGER_VALUE = (byte) 0x00;
    public static final byte COMMAND_STOP = (byte) 0x80;
    public static final byte COMMAND_DOWN = (byte) 0x40;
    public static final byte COMMAND_SELECT = (byte) 0x20;
    public static final byte COMMAND_UP = (byte) 0x10;
    public static final byte COMMAND_PORT_LOW = (byte) 0x00;
    public static final byte COMMAND_LED1 = (byte) 0x01;
    public static final byte COMMAND_LED2 = (byte) 0x02;
    public static final byte COMMAND_LED3 = (byte) 0x04;
    public static final byte COMMAND_LED4 = (byte) 0x08;
    public static final byte COMMAND_LED_ALL = (byte) 0x0F;
    public static final byte COMMAND_LED_NONE = (byte) 0x00;
    private static final String SERIAL_PORT_1 = "/dev/ttyUSB0";
    private static final String SERIAL_PORT_2 = "/dev/ttyUSB1";

    static ArrayList<byte[]> getInitializeMessages() {
        ArrayList<byte[]> initMessages = new ArrayList<byte[]>();
        // set direction port A
        initMessages.add(new byte[]{DEVICE_COMMAND_DIRECTION, DEVICE_PORT1, DEVICE_PORT1_DIRECTION_VALUE});
        // set schmitt-trigger port A
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT1, DEVICE_PORT1_SCHMITT_TRIGGER_VALUE});
        // set all values to 0 port A
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT1, COMMAND_PORT_LOW});

        // set direction port B
        initMessages.add(new byte[]{DEVICE_COMMAND_DIRECTION, DEVICE_PORT2, DEVICE_PORT2_DIRECTION_VALUE});
        // set schmitt-trigger port B
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT2, DEVICE_PORT2_SCHMITT_TRIGGER_VALUE});
        // set all values to 0 port B
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT2, COMMAND_PORT_LOW});

        // set direction port C
        initMessages.add(new byte[]{DEVICE_COMMAND_DIRECTION, DEVICE_PORT3, DEVICE_PORT3_DIRECTION_VALUE});
        // set schmitt-trigger port C
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT3, DEVICE_PORT3_SCHMITT_TRIGGER_VALUE});
        // set all values to 0 port C
        initMessages.add(new byte[]{DEVICE_COMMAND_SCHMITT_TRIGGER, DEVICE_PORT3, COMMAND_PORT_LOW});

        // enable automatic trasmission of pin change
        initMessages.add(new byte[]{DEVICE_COMMAND_ENABLE_MODE_2});

        return initMessages;
    }

    public static enum LEDS {

        LED1,
        LED2,
        LED3,
        LED4,
        LED_ALL,
        LED_NONE,
        UNKNOWN
    }

    public static LEDS getLedIDFromData(byte[] data) {
        if ((data == null) || (data.length == 0)) {
            throw new IllegalArgumentException("command is null or empty");
        }
        int size = data.length -1;
        LEDS receivedLed = LEDS.UNKNOWN;
        
        if (data.length > 1) {
            for (int i = 0; i < size; i++) {
                byte[] tempData = {data[i], data[i + 1]};
                
                LEDS tempLed = getLED(tempData);

                if (((tempLed != LEDS.UNKNOWN) && (tempLed != LEDS.LED_NONE))
                        || ((tempLed != LEDS.UNKNOWN) && (receivedLed == LEDS.UNKNOWN))) {
                    receivedLed = tempLed;
                }
            }
        }
        return receivedLed;
    }

    private static LEDS getLED(byte[] data) {
        if (data.length > 1) {
            byte ledID = data[1];
            ledID = (byte) (ledID & (byte) 0x0F); // filter the lower nibble
            ledID = (byte) (ledID ^ (byte) 0x0F); // invert the received data
            if (ledID == COMMAND_LED1) {
                return LEDS.LED1;
            } else if (ledID == COMMAND_LED2) {
                return LEDS.LED2;
            } else if (ledID == COMMAND_LED3) {
                return LEDS.LED3;
            } else if (ledID == COMMAND_LED4) {
                return LEDS.LED4;
            } else if (ledID == COMMAND_LED_ALL) {
                return LEDS.LED_ALL;
            } else if (ledID == COMMAND_LED_NONE) {
                return LEDS.LED_NONE;
            }
        }
        return LEDS.UNKNOWN;
    }

    public static LEDS getLedIDFromBusID(String busID) {
        if ((busID == null) || (busID.equals(""))) {
            throw new IllegalArgumentException("busID is null or empty");
        }

        return LEDS.values()[getRTSObjectID(busID) - 1];
    }

    private SomfyRTSAddressConverter() {
    }

    public static String getSerialPort(String busID) {
        if ((busID == null) || (busID.equals(""))) {
            throw new IllegalArgumentException("busID is null or empty");
        }

        int busNr = getBusNr(busID);
        switch (busNr) {
            case 1:
            case 2:
            case 3:
                return SERIAL_PORT_1;
            case 4:
            case 5:
            case 6:
                return SERIAL_PORT_2;

            default:
                return "Unknown bus";
        }
    }

    private static int getBusNr(String busID) {
        ArrayList<Integer> ids = getIDs(busID);
        if (ids == null) {
            return 0;
        }
        return ids.get(0);
    }

    private static int getRTSObjectID(String busID) {
        ArrayList<Integer> ids = getIDs(busID);
        if (ids == null) {
            return 0;
        }
        return ids.get(1);
    }

    private static ArrayList<Integer> getIDs(String busID) {
        busID = busID.replace(',', '.');
        StringTokenizer tokens = new StringTokenizer(busID, ".");
        ArrayList<Integer> list = new ArrayList<Integer>(2);
        if (tokens.countTokens() == 2) {
            while (tokens.hasMoreTokens()) {
                list.add(Integer.parseInt(tokens.nextToken()));
            }
            return list;
        }
        return null;
    }

    public static byte[] getPortAllPinsLowCommand(String busID) {
        int busNr = getBusNr(busID);
        byte[] command = new byte[]{(byte) getPrefix(busNr), COMMAND_PORT_LOW};
        return command;
    }

    public static byte[] getSerialCommand(String busID, String actionType) {
        if ((busID == null) || (busID.equals(""))) {
            throw new IllegalArgumentException("busID is null or empty");
        }
        if ((actionType == null) || (actionType.equals(""))) {
            throw new IllegalArgumentException("actionType is null or empty");
        }

        int busNr = getBusNr(busID);
        byte[] command = new byte[]{(byte) getPrefix(busNr), getCommandCode(actionType)};
        return command;
    }

    private static byte getCommandCode(String actionType) {
        if (actionType.equals(RTSActionType.ACTION_UP)) {
            return COMMAND_UP;
        } else if (actionType.equals(RTSActionType.ACTION_DOWN)) {
            return COMMAND_DOWN;
        } else if (actionType.equals(RTSActionType.ACTION_STOP)) {
            return COMMAND_STOP;
        } else if (actionType.equals(RTSActionType.ACTION_SELECT)) {
            return COMMAND_SELECT;
        }

        return (byte) 0xFF;
    }

    public static char getPrefix(int busNr) {
        int devicePort = --busNr % 3;
        switch (devicePort) {
            case 0:
                return DEVICE_PORT1;
            case 1:
                return DEVICE_PORT2;
            case 2:
                return DEVICE_PORT3;
            default:
                return 0xFF;
        }
    }

    public static String getActionType(byte[] data) {
        byte command = data[data.length - 1];
        if (command == COMMAND_UP) {
            return RTSActionType.ACTION_UP;
        } else if (command == COMMAND_DOWN) {
            return RTSActionType.ACTION_DOWN;
        } else if (command == COMMAND_STOP) {
            return RTSActionType.ACTION_STOP;
        } else if (command == COMMAND_SELECT) {
            return RTSActionType.ACTION_SELECT;
        }

        return "Unknown action";
    }
}
