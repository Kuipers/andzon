package org.openremote.controller.protocol.somfy.rts;

import java.security.InvalidParameterException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openremote.controller.command.ExecutableCommand;
import org.openremote.controller.model.sensor.Sensor;
import org.openremote.controller.protocol.EventListener;

public class SomfyRTSCommander implements ExecutableCommand, EventListener, BusOpenCallback {

    private static final String SWITCHSENSOR_ON_STATE = "on";
    private static final String SWITCHSENSOR_OFF_STATE = "off";
    private CommanderHandler mCommnaderHandler;
    private SomfyRTSDriver mSomfyRTSDriver;
    private Sensor mSensor; // Could be null
    protected HandlingCommanderCallback mHandlingCommanderCallback;
    private String mBusID;
    private String mActionType;
    private String mName;

    public SomfyRTSCommander(CommanderHandler handler, SomfyRTSDriver driver) {
        super();
        mCommnaderHandler = handler;
        mSomfyRTSDriver = driver;
    }

    @Override
    public void setSensor(Sensor sensor) {
        Logger.getLogger(SomfyRTSCommander.class.getName()).log(Level.INFO, "register sensor {0}", sensor.getSensorID());
        mSensor = sensor;
    }

    public void setBusID(String busID) {
        mBusID = busID;
    }
    
    public String getBusID() {
        return mBusID;
    }

    public void setActionType(String actionType) {
        mActionType = actionType;
    }
    
    public String getActionType() {
        return mActionType;
    }

    @Override
    public void stop(Sensor sensor) {
    }

    @Override
    public void send() {
        mCommnaderHandler.putCommander(this);
    }

    @Override
    public boolean onOpeningBusFinished(ErrorType error) {
        Logger.getLogger(SomfyRTSCommander.class.getName()).log(Level.INFO, "OpeningBusFinished {0}", error);
        switch (error) {
            case SUCCESS: {
                mSomfyRTSDriver.send(mActionType);
                Logger.getLogger(SomfyRTSCommander.class.getName()).log(Level.INFO, "update sensor status: {0}", SWITCHSENSOR_ON_STATE);
                updateSensor(SWITCHSENSOR_ON_STATE);
                break;
            }
            case FAILED: 
            default: {
                updateSensor(SWITCHSENSOR_OFF_STATE);
            }
        }
        mSomfyRTSDriver.closeBus();
        mHandlingCommanderCallback.onHandlingCommanderFinished(error);
        return true;
    }

    private void updateSensor(String state) {
        if (mSensor != null) {
            Logger.getLogger(SomfyRTSCommander.class.getName()).log(Level.INFO, "update sensor status: {0}", SWITCHSENSOR_OFF_STATE);
            mSensor.update(state);
        }
    }

    public boolean startHandle(HandlingCommanderCallback callback) {
        if (callback == null) {
            throw new InvalidParameterException("callback may not be null");
        }
        Logger.getLogger(SomfyRTSCommander.class.getName()).log(Level.INFO, "startHandle the command. openBusAsync busID = {0}", mBusID);
        mHandlingCommanderCallback = callback;
        return mSomfyRTSDriver.openBusAsync(mBusID, this);
    }

    public void setName(String name) {
        mName = name;
    }
    
    public String getName() {
        return mName;
    }
    
    public CommanderHandler getCommandHandler() {
        return mCommnaderHandler;
    }
    
    public SomfyRTSDriver getSomfyRTSDriver() {
        return mSomfyRTSDriver;
    }
}
