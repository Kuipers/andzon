/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import gnu.io.SerialPort;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mark Singleton
 */
public class SomfyRTSDriver implements TimeOutTimerCallback {

    private static final int SLEEP_TIME = 100; // because the hardware can not write faster then 50ms. So there is a sleep time needed between writes.
    private static final int BAUD_RATE = 9600;
    private static final int DATABITS = SerialPort.DATABITS_8;
    private static final int STOPBITS = SerialPort.STOPBITS_1;
    private static final int PARITY = SerialPort.PARITY_NONE;
    private static final int TIMEOUT_TIME = 1000;
    private String mBusID;
    private SomfyRTSAddressConverter.LEDS mLedToSelect;
    private boolean mIsOpeningBusAsync = false;
    private BusOpenCallback mBusOpenCallback;
    private SerialCommunicator mSerialCommunicator;
    private TimeOutTimer mTimeOutTimer;
    private final SerialDataReceiveObeserver mSerialDataReceiveObeserver = new SerialDataReceiveObeserver() {
        @Override
        public boolean onReceive(byte[] data) {
            boolean result = true;

            if (mIsOpeningBusAsync) {
                Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "received data = {0}", toHexString(data));
                SomfyRTSAddressConverter.LEDS receivedLed = SomfyRTSAddressConverter.getLedIDFromData(data);

                if (receivedLed != SomfyRTSAddressConverter.LEDS.LED_NONE) { // All leds are off, so wait for the next receive

                    if (!mTimeOutTimer.stop()) {
                        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Could not stop timeout timer");
                        result = false;
                    }

                    sleep(SLEEP_TIME);
                    if (!mLedToSelect.equals(receivedLed)) {
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "ledToSelect = {0} received led = {1}", new Object[]{mLedToSelect.toString(), receivedLed.toString()});
                        selectNextRTSObject();
                    } else {
                        mIsOpeningBusAsync = false;
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "ledToSelect = {0} is selected", mLedToSelect.toString());
                        if (result) {
                            mBusOpenCallback.onOpeningBusFinished(ErrorType.SUCCESS);
                        } else {
                            mBusOpenCallback.onOpeningBusFinished(ErrorType.FAILED);
                        }
                    }
                } else {
                    Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "received LED = NONE. wait for new data");
                }
            }
            return result;
        }
    };

    public SomfyRTSDriver(SerialCommunicator communicator) {
        mSerialCommunicator = communicator;
        if (communicator != null) {
            mSerialCommunicator.registerReceiveObserver(mSerialDataReceiveObeserver);
        }
        mTimeOutTimer = new TimeOutTimer();
    }

    public SomfyRTSDriver() {
        this(new SerialCommunicator(BAUD_RATE, DATABITS, STOPBITS, PARITY));
    }

    @Override
    public void onTimeOut() {
        mBusOpenCallback.onOpeningBusFinished(ErrorType.FAILED);
        closeBus();
    }

    public boolean send(String actionType) {
        boolean result = true;
        byte[] command = SomfyRTSAddressConverter.getSerialCommand(mBusID, actionType);
        if (!mSerialCommunicator.write(command, command.length)) {
            result = false;
        }

        sleep(SLEEP_TIME);
        command = SomfyRTSAddressConverter.getPortAllPinsLowCommand(mBusID);
        if (!mSerialCommunicator.write(command, command.length)) {
            result = false;
        }

        return result;
    }

    public boolean openBusAsync(String busID, BusOpenCallback callback) {
        mIsOpeningBusAsync = true;
        mBusID = busID;
        mBusOpenCallback = callback;
        mLedToSelect = SomfyRTSAddressConverter.getLedIDFromBusID(mBusID);
        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.INFO, "start opening the bus async busID = {0} select = {1}", new Object[]{mBusID, mLedToSelect.toString()});

        String serialPort = SomfyRTSAddressConverter.getSerialPort(mBusID);
        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.INFO, "try open port ", serialPort);
        if (!mSerialCommunicator.openPort(serialPort)) {
            Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Could not open serial port {0}", serialPort);
            mBusOpenCallback.onOpeningBusFinished(ErrorType.FAILED);
            return false;
        }
        sleep(SLEEP_TIME);

        if (!initializeDevice()) {
            Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Could not initialize the device");
            mBusOpenCallback.onOpeningBusFinished(ErrorType.FAILED);
            return false;
        }
        sleep(SLEEP_TIME);

        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.INFO, "try select next led", serialPort);
        if (!selectNextRTSObject()) {
            Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Could not select the wright RTS object {0}", serialPort);
            mBusOpenCallback.onOpeningBusFinished(ErrorType.FAILED);
            return false;
        }
        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Finished opening bus async");
        return true;
    }

    private synchronized boolean selectNextRTSObject() {

        if (!mTimeOutTimer.start(TIMEOUT_TIME, this)) {
            Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Could not start timeout timer");
            return false;
        }

        byte[] command = SomfyRTSAddressConverter.getSerialCommand(mBusID, RTSActionType.ACTION_SELECT);
        mSerialCommunicator.write(command, command.length);
        sleep(SLEEP_TIME);
        command = SomfyRTSAddressConverter.getPortAllPinsLowCommand(mBusID);
        mSerialCommunicator.write(command, command.length);
        sleep(SLEEP_TIME);
        return true;
    }

    public boolean closeBus() {
        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "close bus");
        mSerialCommunicator.closePort();
        return true;
    }

    public boolean terminate() {
        Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "terminate");
        mSerialCommunicator.unregisterReceiveObserver(mSerialDataReceiveObeserver);
        closeBus();
        return true;
    }

    private boolean initializeDevice() {
        ArrayList<byte[]> initMessagesList = SomfyRTSAddressConverter.getInitializeMessages();
        for (byte[] message : initMessagesList) {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Initialize device: writing message: {0}", new String(message));
            mSerialCommunicator.write(message, message.length);
        }

        return true;
    }

    private void sleep(int millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException ex) {
            Logger.getLogger(SomfyRTSDriver.class.getName()).log(Level.SEVERE, "Something bad happend when thread was sleeping...", ex);
        }
    }

    public static String toHexString(byte[] ba) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < ba.length; i++) {
            if (i % 2 == 0) {
                str.append(' ');
            }
            if ((char) ba[i] == 'a') {
                str.append('a');
            } else {
                str.append(String.format("%x", ba[i]));
            }
        }
        return str.toString();
    }

    public static String fromHexString(String hex) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
        }
        return str.toString();
    }
}
