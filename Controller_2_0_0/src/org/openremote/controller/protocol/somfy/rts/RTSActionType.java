/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

/**
 *
 * @author mark
 */
public interface RTSActionType {
    public static final String ACTION_STOP = "stop";
    public static final String ACTION_DOWN = "down";
    public static final String ACTION_SELECT = "select";
    public static final String ACTION_UP = "up";
}
