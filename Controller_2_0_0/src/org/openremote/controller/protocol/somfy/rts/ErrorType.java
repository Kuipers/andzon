package org.openremote.controller.protocol.somfy.rts;

public enum ErrorType {

    SUCCESS,
    FAILED,
    INVALID
}
