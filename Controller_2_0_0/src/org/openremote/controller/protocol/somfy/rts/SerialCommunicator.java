/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import gnu.io.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.util.ByteArrayBuffer;

/**
 *
 * @author mark Singleton
 */
public class SerialCommunicator {
    
    private Enumeration mPortList;
    private CommPortIdentifier mPortId;
    private SerialPort mSerialPort;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    
    private int mBaudRate, mDatabits, mStopbits, mParity;

    protected volatile ArrayList<SerialDataReceiveObeserver> mOnRevieveObservers;
    private volatile Thread mReceiveThread;
        
    public SerialCommunicator(int baudRate, int databits, int stopbits, int parity) {
        mBaudRate = baudRate;
        mDatabits = databits;
        mStopbits = stopbits;
        mParity = parity;
        
        mOnRevieveObservers = new ArrayList<SerialDataReceiveObeserver>();
    }

    /**
     * check of je exceptions terug moet geven.
     * @param serialPort
     * @return
     * @throws InvalidParameterException 
     */
    public boolean openPort(String serialPort) throws InvalidParameterException {
        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Start opening the port {0}", serialPort);
        if ((serialPort == null) || serialPort.isEmpty()) {
            throw new InvalidParameterException("Comport cannot be empty");
        }

        // find serial port
        mPortList = CommPortIdentifier.getPortIdentifiers();
        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Port list has more ports: {0}", mPortList.hasMoreElements());
        while (mPortList.hasMoreElements()) {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Go through port Identifiers list");
	    mPortId = (CommPortIdentifier) mPortList.nextElement();

	    if (mPortId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "{0} is a serial port", mPortId);

		if (mPortId.getName().equals(serialPort)) {
                    Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "{0} found", serialPort);

		    try {
			mSerialPort = (SerialPort) mPortId.open("SomfyRTS", 2000);
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "{0} is now open", serialPort);
		    } catch (PortInUseException ex) {
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
		    } 

		    try {
			mOutputStream = mSerialPort.getOutputStream();
                        mInputStream = mSerialPort.getInputStream();
		    } catch (IOException ex) {
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }

		    try {
			mSerialPort.setSerialPortParams(mBaudRate, 
						       mDatabits, 
						       mStopbits, 
						       mParity);
		    } catch (UnsupportedCommOperationException ex) {
                        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
	

		    try {
		    	mSerialPort.notifyOnOutputEmpty(true);
		    } catch (Exception e) {
			System.out.println("Error setting event notification");
			System.out.println(e.toString());
			return false;
		    }
                    
                    mReceiveThread = new Thread(mSerialReceiveRunnable);
                    mReceiveThread.start();
                    Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Port is ready", serialPort);
                    return true;
		} 
	    } 
	}
        return false;
    }
    
    public boolean closePort() {
        boolean result = true;

        // flush and close the outputstream
        if (mOutputStream != null) {
            try {
                Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Close port");
                mOutputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            result = false;
        }

        // close the serial port
        if (mSerialPort != null) {
            mSerialPort.close();
        } else {
            result = false;
        }
        
        if (mReceiveThread != null) {
            // this will stop the thread look in the Runnable
            mReceiveThread = null;
        }

        return result;
    }
    
    public boolean registerReceiveObserver(SerialDataReceiveObeserver observer) {
        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Register observer");
        return mOnRevieveObservers.add(observer);
    }
    
    public boolean unregisterReceiveObserver(SerialDataReceiveObeserver observer) {
        Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Unregister observer");
        return mOnRevieveObservers.remove(observer);
    }
    
    public boolean write(String data) {
        try {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Writing \"{0}\" to {1}", new Object[] {data, mSerialPort.getName()});
            mOutputStream.write(data.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public synchronized boolean write(byte[] bytes, int length) {
        try {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Writing bytes to {0}", mSerialPort.getName());
            mOutputStream.write(bytes, 0, length);
        } catch (IOException ex) {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean terminate() {
        mOnRevieveObservers.clear();
        return closePort();
    }
    
    private final Runnable mSerialReceiveRunnable = new Runnable() {
        @Override
        public void run() {
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Receiver thread started");
            Thread thisTread = Thread.currentThread();
            int numBytes;
            byte[] readBuffer = new byte[512];
            ByteArrayBuffer arrayBuffer = new ByteArrayBuffer(512);
            while (mReceiveThread == thisTread) {
                try {
                    while (mInputStream.available() > 0) {
                        numBytes = mInputStream.read(readBuffer);
                        arrayBuffer.append(readBuffer, 0, numBytes);
                    }
                    if (arrayBuffer.length() > 0) {
                        notifyObservers(arrayBuffer.toByteArray());
                    }
                    readBuffer = new byte[512];
                    arrayBuffer.clear();
                } catch (IOException ex) {
                    Logger.getLogger(SerialCommunicator.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
            Logger.getLogger(SerialCommunicator.class.getName()).log(Level.INFO, "Serial receiver thread exited");
        }
    };
    
    protected void notifyObservers(byte[] data) {
        for (SerialDataReceiveObeserver observer: mOnRevieveObservers) {
            observer.onReceive(data);
        }
    }
}