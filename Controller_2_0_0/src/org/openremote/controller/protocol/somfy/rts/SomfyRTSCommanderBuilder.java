package org.openremote.controller.protocol.somfy.rts;

import java.util.List;
import org.jdom.Element;
import org.openremote.controller.command.Command;
import org.openremote.controller.command.CommandBuilder;

/**
 * @author mark
 *
 * Builds on the initialisation of the system all needed SomfyRTSCommander
 * objects who are defined with the Designer. The SomfyRTSCommander are
 * generated regarding the definition in controller.xml
 *
 */
public class SomfyRTSCommanderBuilder implements CommandBuilder {

    private SomfyRTSDriver mSomfyRTSDriver;
    private final CommanderHandler mCommanderHandler = new CommanderHandler();

    public SomfyRTSCommanderBuilder(SerialCommunicator serialCommunicator) {
        super();
        mSomfyRTSDriver = new SomfyRTSDriver(serialCommunicator);
        mCommanderHandler.startHandling();
    }
    
    public SomfyRTSCommanderBuilder() {
        super();
        mSomfyRTSDriver = new SomfyRTSDriver();
        mCommanderHandler.startHandling();
    }
    
    public CommanderHandler getCommanderHandler() {
        return mCommanderHandler;
    }

    @Override
    public Command build(Element element) {
        SomfyRTSCommander commander = new SomfyRTSCommander(mCommanderHandler, mSomfyRTSDriver);
        List<Element> propertyEles = element.getChildren("property", element.getNamespace());
        for (Element ele : propertyEles) {
            if ("busID".equals(ele.getAttributeValue("name"))) {
                commander.setBusID(ele.getAttributeValue("value"));
            } else if ("actionType".equals(ele.getAttributeValue("name"))) {
                commander.setActionType(ele.getAttributeValue("value"));
            }
        }
        return commander;
    }
}

