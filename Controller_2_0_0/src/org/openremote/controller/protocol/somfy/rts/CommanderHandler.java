/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mark
 */
public class CommanderHandler implements HandlingCommanderCallback {

    protected SomfyRTSCommander mCurrentCommander;
    private Thread mProcessQueueThread;
    private final LinkedBlockingQueue<SomfyRTSCommander> mRTSCommandersQueue = new LinkedBlockingQueue<SomfyRTSCommander>();
    private final Runnable mProcessingRunnable = new Runnable() {

        @Override
        public void run() {
            Thread thisThread = Thread.currentThread();
            while (thisThread == mProcessQueueThread) {
                if (mCurrentCommander == null) {
                    try {
                        Logger.getLogger(CommanderHandler.class.getName()).log(Level.INFO, "Check the queue");
                        mCurrentCommander = mRTSCommandersQueue.take();
                        Logger.getLogger(CommanderHandler.class.getName()).log(Level.INFO, "Handle current commander");
                        if (!mCurrentCommander.startHandle(CommanderHandler.this)) {
                            // if the handling failes remove the message from the queue and take the next message.
                            mCurrentCommander = null;
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(CommanderHandler.class.getName()).log(Level.SEVERE, "Something bad happend while processing the RTSCommander queue", ex);
                        mCurrentCommander = null;
                    }
                } else {
                    sleep(100);
                }
            }
        }
        
        private void sleep(int millisec) {
            try {
                Thread.sleep(millisec);
            } catch (InterruptedException ex) {
                Logger.getLogger(CommanderHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };
    
    public CommanderHandler() {
    }
    
    public boolean startHandling() {
        if (mProcessQueueThread == null) {
            mProcessQueueThread = new Thread(mProcessingRunnable);
            mProcessQueueThread.setName("Processing Queue thread");
            mProcessQueueThread.start();
            Logger.getLogger(CommanderHandler.class.getName()).log(Level.INFO, "Start processing the queue");
        }
        return true;
    }
    
    public boolean stopHandling() {
        mProcessQueueThread = null;
        return true;
    }
    
    public boolean putCommander(SomfyRTSCommander commander) {
        synchronized(mRTSCommandersQueue) {
            try {
                mRTSCommandersQueue.put(commander);
                Logger.getLogger(CommanderHandler.class.getName()).log(Level.INFO, "Commander added to queue");
            } catch (InterruptedException ex) {
                Logger.getLogger(CommanderHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return true;
    }

    @Override
    public boolean onHandlingCommanderFinished(ErrorType error) {
        // commnader is finished so release the object
        Logger.getLogger(CommanderHandler.class.getName()).log(Level.INFO, "handling commander finished");
        mCurrentCommander = null;
        return true;
    }

    public boolean terminate() {
        synchronized(mRTSCommandersQueue) {
            mRTSCommandersQueue.clear();
        }
        stopHandling();
        return true;
    }
    
    public int getQueueCount(){
        int result;
        synchronized(mRTSCommandersQueue) {
            result = (mCurrentCommander != null)? 1 : 0;
            result += mRTSCommandersQueue.size();
        }
        return result;
    }
}
