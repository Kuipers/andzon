/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openremote.controller.protocol.somfy.rts;

/**
 *
 * @author mark
 */
public interface SerialDataReceiveObeserver {

    public boolean onReceive(byte[] data);
}
